# Create your models here.

#!/usr/bin/env python
# -*- coding:utf-8 -*-

from models import Meal, Country, Place
from converters import create_allergens_string

def add_meal(place_id, name, 
	alternative_name, lbm = ([],[]),
	mi = ([],[]), lbs = ([],[]), si = ([],[]),
	u = ([],[])):
	place = Place.objects.get(place_id)
	lbm_string = create_allergens_string(lbm[0], lbm[1])
	mi_string = create_allergens_string(mi[0], mi[1])
	lbs_string = create_allergens_string(lbs[0], lbs[1])
	si_string = create_allergens_string(si[0], si[1])
	u_string = create_allergens_string(u[0], u[1],sep=";")
	meal = Meal.object.create(meal_name = name, 
		alternative_names = alternative_name,
		place = place,
		listed_by_manufacturer = lbm_string,
		manufacturer_info = mi_string,
		checked_by_societies = lbs_string,
		societies_info = si_string,
		reports_unsafe = u_string
		)
	print(meal)
