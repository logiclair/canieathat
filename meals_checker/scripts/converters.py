# Create your models here.

#!/usr/bin/env python
# -*- coding:utf-8 -*-

"""
Module for differnt types of converters
for the database.
"""

def create_allergens_string(inds_list, val_list, n_allergens = 14, sep=""):
	"""Creates a string of 0s, 1s and 2s from the given list of allergens
		with the given separator between symbols.

	Parameters
	------------
	a_list: list 
		List of integers representing allregens ids
		as specified in meals_checker/static/assets/allergens.json
	
	val_list: list
		list of allergen values:
		0 if the allergen is present
		1 if the the product can contain traces 
		2 if the allergen is NOT present


	n_allergens: int
		Number of allergens. Defines the length of the resulting string.

	sep: str
		Separator for the values.

	Returns
	-----------
	str
		String representation of the allergens for the database.

	Examples
	----------
	>>> create_allergens_string([1,2],[1,1])
	'01100000000000'

	>>> create_allergens_string([0,4], [1,1], n_allergens = 10)
	'1000100000'

	>>> create_allergens_string([0,4], [1,1], sep=";")
	'1;0;0;0;1;0;0;0;0;0;0;0;0;0'

	>>> create_allergens_string([0,4,5], [1,2,1], sep=";")
	'1;0;0;0;2;1;0;0;0;0;0;0;0;0'

	"""
	j = 0
	result = ""
	for i in range(n_allergens):
		if i in inds_list:
			result += str(val_list[j]) + sep
			j+=1
		else:
			result += "0" + sep
	if len(sep) > 0:
		return result[:-len(sep)]
	return result