$(document).ready(function (){
    // Hide navbar at mobile devices when clicking something else
    $(document).click(function (event){
        let clickover = $(event.target);
        let navbar = $(".navbar-collapse");
        let isOpened = navbar.hasClass("in");
        if (isOpened === true && !clickover.hasClass("navbar-toggle")) { navbar.collapse('hide'); }
    });
    // Fix for navbar-brand zooming on click at touch devices
    $('.navbar-brand').on('click touchend', function(e) { return false; });

    // Обработчик нажатия на кнопку Help
    $("#navbar_link_help").click(function(){ return false; });
});