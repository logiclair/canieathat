// Заготовки для выводимого HTML
const user_allergens_list_element_template = "<a href='#' class='allergens_list_element list-group-item'>?allergen_name<span class='glyphicon glyphicon-remove'></span></a>";
const search_result_template = "<a href='#' class='search_result_element list-group-item'>?search_result<span class='glyphicon glyphicon-chevron-up'></span></a>";
const allergens_search_result_template = "<a href='#' class='search_result_element list-group-item'>?search_result<span class='glyphicon glyphicon-plus'></span></a>";
const modal_collapsable_template = "<div class='panel-group'><div class='panel panel-default'><div class='panel-heading'><a data-toggle='collapse' href='#?collapse_id'><h4 class='panel-title'>?collapse_heading</h4></a></div><div id='?collapse_id' class='panel-collapse collapse'><div class='panel-body'>?collapse_body</div></div></div></div>";
const checkup_histroy_element_template = "<a href='#' class='checkup_history_list_element list-group-item' onclick='?onclick'>?checkup_text</a>"
// Глобальные переменные, используемые на данной странице
const CSRFToken = document.getElementsByName("csrfmiddlewaretoken")[0].value; // Получаем CSRFToken из Cookies для будущей отправки и получения данных с сервера
let street_or_metrost_row_original_height;
// const CSRFToken = Cookies.get("csrftoken");
let database = {};                          // Храним в памяти полный список аллергенов, по которым выполняется поиск при добавлении
let user_allergens_list_html = "";          // Храним в памяти HTML списка аллергенов пользователя, что бы потом не выполнять лишние действия
let user_allergens_list_dictionary = [];    // Храним список аллергенов пользователя в памяти, что бы потом лишний раз не извлекать его из HTML
// Настройки фази-поиска
const fuse_options = { keys: ["name"], shouldSort: true, matchAllTokens: true, threshold: 0.2, location: 0, distance: 100, minMatchCharLength: 1, maxPatternLength: 32 };
let allergens_fuse_search;
let allergens_search_lock = false;
let allergens_search_upordown = false;
let allergens_search_upordown_event;
let allergens_loaded = false;
let city_old;
let meal_city_loaded = false;
let meal_street_search_array;
let meal_street_fuse_search;
let meal_street_search_lock = false;
let meal_street_search_upordown = false;
let meal_street_search_upordown_event;
let meal_street_loaded = false;
let street_old;
let meal_metrost_search_array;
let meal_metrost_fuse_search;
let meal_metrost_search_lock = false;
let meal_metrost_search_upordown = false;
let meal_metrost_search_upordown_event;
let meal_metrost_loaded = false;
let metrost_old;
let meal_place_search_array;
let meal_place_fuse_search;
let meal_place_search_lock = false;
let meal_place_search_upordown = false;
let meal_place_search_upordown_event;
let meal_place_loaded = false;
let place_name_old;
let meal_name_search_array;
let meal_name_fuse_search;
let meal_name_search_lock = false;
let meal_name_search_upordown = false;
let meal_name_search_upordown_event;
let meal_name_loaded = false;

// Модальное окно
let result_modal = document.getElementById('result_window');
const safe_flag = "1";

{   // Очистка хранилища при изменении структуры
    const localStorage_version_expected = "3";      // Смена версии локального хранилища приводит к очистке данных пользователя
    let localStorage_version = localStorage.getItem("localstorage_version");
    if (localStorage_version != localStorage_version_expected)
    {
        Cookies.remove("user_allergens");
        localStorage.removeItem('user_allergens');
        localStorage.removeItem('preferences');
        localStorage.removeItem('checkup_history');
        localStorage.removeItem('database');
        localStorage.setItem("localstorage_version", localStorage_version_expected)
    }
}

// История
let checkup_history = JSON.parse(localStorage.getItem('checkup_history'));
const checkup_history_structure = { "entries": [] };
if (checkup_history == null || checkup_history.length == 0) { checkup_history = checkup_history_structure; }
let checkup_number = checkup_history["entries"].length;     // {"entries": [{}, {}, {}]}

function resetCheckupHistory()
{
    checkup_history = checkup_history_structure;
    localStorage.setItem('checkup_history', JSON.stringify(checkup_history));
    $("#checkup_list").html("");
    // $("#no_history_message").show();
}

// По окончанию парсинга страницы, привязываем обработчики событий и загружаем нужные данные
$(document).ready(function() {
    {   // Загружаем список аллергенов пользователя
        let user_allergens_JSON = localStorage.getItem("user_allergens"); // Вызов функции из библиотеки - достаём JSON.
        if (user_allergens_JSON != null && user_allergens_JSON.length > 0) { user_allergens_list_dictionary = JSON.parse(user_allergens_JSON); } // Если локальное хранилище не пустое, переносим это что-то в список аллергенов в памяти
    }

    if (checkup_number == 0) { /* $('#no_history_message').show(); */ } else {
        let checkup_history_html = "";
        for (checkup_number_i in checkup_history["entries"])
        {
            if(!checkup_history["entries"].hasOwnProperty(checkup_number_i)) continue;
            checkup_history_html += checkup_histroy_element_template.replace(/\?checkup_text/gi, checkup_history["entries"][checkup_number_i].product_name + "<span class='checkup_history_element_status " + (checkup_history["entries"][checkup_number_i].safe ? 'result_text_ok' : 'result_text_danger') + "'>" + checkup_history["entries"][checkup_number_i].result + "</span>").replace(/\?onclick/gi,
            "displayCheckupResult(checkup_history[\"entries\"][" + checkup_number_i + "])");
        }
        $("#checkup_list").html(checkup_history_html);
    }

    // Если в списке аллергенов в памяти что-то есть, добавляем его в HTML
    if (user_allergens_list_dictionary != null && user_allergens_list_dictionary.length > 0)
    {
        for (let saved_allergen_number in user_allergens_list_dictionary)
        {
            if(!user_allergens_list_dictionary.hasOwnProperty(saved_allergen_number)) continue;
            user_allergens_list_html += user_allergens_list_element_template.replace(/\?allergen_name/gi, user_allergens_list_dictionary[saved_allergen_number]);
        }
        $("#allergens_list").html(user_allergens_list_html);
        $("#allergens_list > .allergens_list_element").click(function() { return false; });
        addAlergenRemoveEvents();
    }
    else { $("#no_allergens_message").show(); } // Если список аллергенов пуст, говорим об этом пользователю

    loadDatabase();   // Обновляем базу данных с полным списком аллергенов

    // $("#is_kosher").click(function() {
    //     let preferences = { is_kosher:false, is_halal:false };
    //     preferences_to_load = localStorage.getItem("preferences");
    //     localStorage.setItem("preferences", );
    // });

    // $("#is_halal").click(function() {
        
    // });

    street_or_metrost_row_original_height = $("#street_or_metrost_row").height();

    // Живой поиск
    $("#allergens_search").addClear({ symbolClass: "glyphicon glyphicon-remove", clearOnEscape: false, onClear: function() { $("#allergens_search_results").html(""); $("#allergens_search").change(); }});
    $("#allergens_search").keyup(function(e) {
        if ((e.keyCode != 13) && (e.keyCode != 27) && (e.keyCode != 38 && e.keyCode != 40) && (allergens_search_lock == false))
        {
            let search_filed_value = $(this).val();     // Добываем значения поля поиска
            let search_result = allergens_fuse_search.search(search_filed_value);
            let search_result_html = "";                // HTML для вывода найденных аллергенов

            // Формируем HTML для вывода
            if (search_result != null && search_result.length > 0)    // Проверяем, получили ли мы вообще хоть какой-то список
            { 
                for (let search_result_number in search_result)  // Добываем элементы из списка и формируем HTML подсказки
                {
                    if(!search_result.hasOwnProperty(search_result_number)) continue;
                    search_result_html += allergens_search_result_template.replace(/\?search_result/gi, search_result[search_result_number].name);
                }
            }
            $("#allergens_search_results").html(search_result_html);    // Добавляем подготовленную разметку на страницу
            $("#allergens_search_results > .search_result_element").on("click touchend", function(e) { // Добавляем обработчик нажатия на элемент из списка подсказок. Различаем click и touch нажатия.
                $("#allergens_search").val($(this).text());
                $("#allergens_search_results").html("");
                if (e.type == 'click') {
                    // $("#btn_add_allergen").click();
                    $("#allergens_search").focus();                    
                }
                return false;   // Это нужно для игнорирования стандартного действия по нажатию на ссылку (<a>)
            });
        } else if (e.keyCode == 27) {
            $("#allergens_search_results").html("");
        }
    });
    // Обработка нажатия Enter в поле поиска аллергенов
    $("#allergens_search").keypress(function(e) {
        if(e.keyCode == 13)
        {
            let allergen_name = $(this).val();   // Получаем значения поля поиска
            if (allergen_name != null && allergen_name.length > 0)  // Если поле не пустое, обрабатываем его
            {
                if ($('#allergens_search_results').children() != null && $('#allergens_search_results').children().length > 0)
                {   // Если живой поеск нам что-то подсказывает, выбираем по нажатию на Enter первую из подсказок. Так же, можно выбирать элементы через Tab
                    $(this).val($('#allergens_search_results').children().first().text());
                }
                $("#btn_add_allergen").click();
            }
            else    // Если поле пустое, сообщаем пользователю что он не прав
            {
                console.log("Please choose your allergens!");
            }
            e.preventDefault(); // Предотвращаем отправку формы по нажатию на enter
        }
    });
    // Обработка событий нажатия стрелок вниз и вверх для поля поиска
    $("#allergens_search").keydown(function(e) {
        if (e.keyCode == 38 || e.keyCode == 40) {
            if (allergens_search_lock == false) 
            {
                if ($(this).val() == null || $(this).val() == "")
                {
                    let search_result_html = "";
                    if (database.allergens != null && database.allergens.length > 0)    // Проверяем, получили ли мы вообще хоть какой-то список
                    { 
                        for (let search_result_number in database.allergens)  // Добываем элементы из списка и формируем HTML подсказки
                        {
                            if(!database.allergens.hasOwnProperty(search_result_number)) continue;
                            if(database.allergens[search_result_number].lang != "en") continue;
                            search_result_html += allergens_search_result_template.replace(/\?search_result/gi, database.allergens[search_result_number].name);
                        }
                    }
                    $("#allergens_search_results").html(search_result_html);    // Добавляем подготовленную разметку на страницу
                    $("#allergens_search_results > .search_result_element").on("click touchend", function(e) { // Добавляем обработчик нажатия на элемент из списка подсказок. Различаем click и touch нажатия.
                        $("#allergens_search").val($(this).text());
                        $("#allergens_search_results").html("");
                        if (e.type == 'click') {
                            // $("#btn_add_allergen").click();
                            $("#allergens_search").focus();
                        }
                        return false;   // Это нужно для игнорирования стандартного действия по нажатию на ссылку (<a>)
                    });
                }
                switch(e.keyCode) {
                    case 38:    // Up
                        if ($("#allergens_search_results").children() != null && $("#allergens_search_results").children().length > 0 ) { $("#allergens_search_results").children().last().focus(); }
                        e.preventDefault();
                        break;
                    case 40:    // Down
                        if ($("#allergens_search_results").children() != null && $("#allergens_search_results").children().length > 0 ) { $("#allergens_search_results").children().first().focus(); }
                        e.preventDefault();
                        break;
                }
            }
            else
            {
                allergens_search_upordown = true;
                allergens_search_upordown_event = e.keyCode;
            }
        } else {
            allergens_search_upordown = false;
        }
    });
    $("#allergens_search_results").keydown(function(e) {
        switch(e.keyCode) {
            case 38:    // Up
                if ($(':focus').prev() != null && $(':focus').prev().length > 0) { $(':focus').prev().focus(); } else { $("#allergens_search").focus(); }
                e.preventDefault();
                break;
            case 40:    // Down
                if ($(':focus').next() != null && $(':focus').next().length > 0) { $(':focus').next().focus(); } else { $("#allergens_search").focus(); }
                e.preventDefault();
                break;
        }
    });
    $("#allergens_search_results").keyup(function(e){
        if (e.keyCode == 27) {
            $("#allergens_search").focus();
            $("#allergens_search_results").html("");
        }
    });
    $("#allergens_search").focusin(function(){
        if ($(this).val() != null && $(this).val().length > 0) {
            $("#allergens_search").trigger(jQuery.Event("keyup"));
        }
    });
    $("#allergens_search").focusout(function() {
        setTimeout(function() {
            if ($("#allergens_search_results").find(":focus").length == 0)
            {
                $("#allergens_search_results").html("");
            }
        }, 10);
    });
    $("#allergens_search_results").focusout(function() {
        var $this = $(this);
        setTimeout(function() {
            if (($this.find(":focus").length == 0) && (!$("#allergens_search").is(":focus")))
            {
                $("#allergens_search_results").html("");
            }
        }, 10);
    });

    // Живой поиск для Meals > Street
    $("#meal_street_search").focusin(function(){
        const city = $("#meal_city").val();
        if (city != city_old)
        {
            city_old = city;
            if (city != null && city.length != 0)
            {
                meal_street_search_lock = true;
                $.ajax({
                    type: "POST",
                    url: urls.streets_search,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    headers: { "X-CSRFToken": CSRFToken },
                    data: JSON.stringify({ "country" : $("#meal_country").val(), "city": $("#meal_city").val() }),
                    success: function(data, textStatus, jqXHR) {
                        meal_street_search_array = [];
                        for (let search_element_number in data.streets) {
                            if(!data.streets.hasOwnProperty(search_element_number)) continue;
                            meal_street_search_array.push({ "name": data.streets[search_element_number].street_name_en });
                        }
                        meal_street_fuse_search = new Fuse(meal_street_search_array, fuse_options);
                        meal_street_search_lock = false;
                        if (meal_street_search_upordown) {
                            let event = jQuery.Event("keydown");
                            event.keyCode = meal_street_search_upordown_event;
                            meal_street_search_upordown = false;
                            $("#meal_street_search").trigger(event);
                        } else {
                            $("#meal_street_search").keyup();
                        }
                    }
                });
            }
        }
    });
    $("#meal_street_search").addClear({ symbolClass: "glyphicon glyphicon-remove", clearOnEscape: false, onClear: function() { $("#meal_street_search_results").html(""); $("#meal_street_search").change(); }});
    $("#meal_street_search").keyup(function(e) {
        if ((e.keyCode != 13) && (e.keyCode != 27) && (e.keyCode != 38 && e.keyCode != 40) && (meal_street_search_lock == false))
        {
            let search_filed_value = $(this).val();     // Добываем значения поля поиска
            let search_result = meal_street_fuse_search.search(search_filed_value);
            let search_result_html = "";                // HTML для вывода найденных аллергенов

            // Формируем HTML для вывода
            if (search_result != null && search_result.length > 0)    // Проверяем, получили ли мы вообще хоть какой-то список
            { 
                for (let search_result_number in search_result)  // Добываем элементы из списка и формируем HTML подсказки
                {
                    if(!search_result.hasOwnProperty(search_result_number)) continue;
                    search_result_html += search_result_template.replace(/\?search_result/gi, search_result[search_result_number].name);
                }
            }
            $("#meal_street_search_results").html(search_result_html);    // Добавляем подготовленную разметку на страницу
            $("#meal_street_search_results > .search_result_element").on("click touchend", function(e) { // Добавляем обработчик нажатия на элемент из списка подсказок. Различаем click и touch нажатия.
                $("#meal_street_search").val($(this).text());
                $("#meal_street_search_results").html("");
                return false;   // Это нужно для игнорирования стандартного действия по нажатию на ссылку (<a>)
            });
        } else if (e.keyCode == 27) {
            $("#meal_street_search_results").html("");
        }
    });
    // Обработка нажатия Enter в поле поиска аллергенов
    $("#meal_street_search").keypress(function(e) {
        if(e.keyCode == 13)
        {
            let allergen_name = $(this).val();   // Получаем значения поля поиска
            if (allergen_name != null && allergen_name.length > 0)  // Если поле не пустое, обрабатываем его
            {
                if ($('#meal_street_search_results').children() != null && $('#meal_street_search_results').children().length > 0)
                {   // Если живой поеск нам что-то подсказывает, выбираем по нажатию на Enter первую из подсказок. Так же, можно выбирать элементы через Tab
                    $(this).val($('#meal_street_search_results').children().first().text());
                }
                $("#meal_street_search_results").html("");
            }
            else    // Если поле пустое, сообщаем пользователю что он не прав
            {
                console.log("Please choose your allergens!");
            }
            e.preventDefault(); // Предотвращаем отправку формы по нажатию на enter
        }
    });
    // Обработка событий нажатия стрелок вниз и вверх для поля поиска
    $("#meal_street_search").keydown(function(e) {
        if (e.keyCode == 38 || e.keyCode == 40)
        {
            if (meal_street_search_lock == false)
            {
                if ($(this).val() == null || $(this).val() == "")
                {
                    let search_result_html = "";
                    if (meal_street_search_array != null && meal_street_search_array.length > 0)    // Проверяем, получили ли мы вообще хоть какой-то список
                    { 
                        for (let search_result_number in meal_street_search_array)  // Добываем элементы из списка и формируем HTML подсказки
                        {
                            if(!meal_street_search_array.hasOwnProperty(search_result_number)) continue;
                            // if(meal_street_search_array[search_result_number].lang != "en") continue;
                            search_result_html += search_result_template.replace(/\?search_result/gi, meal_street_search_array[search_result_number].name);
                        }
                    }
                    $("#meal_street_search_results").html(search_result_html);    // Добавляем подготовленную разметку на страницу
                    $("#meal_street_search_results > .search_result_element").on("click touchend", function(e) { // Добавляем обработчик нажатия на элемент из списка подсказок. Различаем click и touch нажатия.
                        $("#meal_street_search").val($(this).text());
                        $("#meal_street_search_results").html("");
                        return false;   // Это нужно для игнорирования стандартного действия по нажатию на ссылку (<a>)
                    });
                }
                switch(e.keyCode) {
                    case 38:    // Up
                        if ($("#meal_street_search_results").children() != null && $("#meal_street_search_results").children().length > 0 ) { $("#meal_street_search_results").children().last().focus(); }
                        e.preventDefault();
                        break;
                    case 40:    // Down
                        if ($("#meal_street_search_results").children() != null && $("#meal_street_search_results").children().length > 0 ) { $("#meal_street_search_results").children().first().focus(); }
                        e.preventDefault();
                        break;
                }
            }
            else
            {
                meal_street_search_upordown = true;
                meal_street_search_upordown_event = e.keyCode;
            }
        } else {
            meal_street_search_upordown = false;
        }
    });
    $("#meal_street_search_results").keydown(function(e) {
        switch(e.keyCode) {
            case 38:    // Up
                if ($(':focus').prev() != null && $(':focus').prev().length > 0) { $(':focus').prev().focus(); } else { $("#meal_street_search").focus(); }
                e.preventDefault();
                break;
            case 40:    // Down
                if ($(':focus').next() != null && $(':focus').next().length > 0) { $(':focus').next().focus(); } else { $("#meal_street_search").focus(); }
                e.preventDefault();
                break;
        }
    });
    $("#meal_street_search_results").keyup(function(e){
        if (e.keyCode == 27) {
            $("#meal_street_search").focus();
            $("#meal_street_search_results").html("");
        }
    });
    $("#meal_street_search").focusin(function(){
        if ($(this).val() != null && $(this).val().length > 0) {
            $("#meal_street_search").trigger(jQuery.Event("keyup"));
        }
    });
    $("#meal_street_search").focusout(function() {
        setTimeout(function() {
            if ($("#meal_street_search_results").find(":focus").length == 0)
            {
                $("#meal_street_search_results").html("");
            }
        }, 10);
    });
    $("#meal_street_search_results").focusout(function() {
        var $this = $(this);
        setTimeout(function() {
            if (($this.find(":focus").length == 0) && (!$("#meal_street_search").is(":focus")))
            {
                $("#meal_street_search_results").html("");
            }
        }, 10);
    });

    // Живой поиск для Meals > Metrostation
    $("#meal_metrost_search").focusin(function(){
        const city = $("#meal_city").val();
        if (city != city_old)
        {
            city_old = city;
            if (city != null && city.length != 0)
            {
                meal_metrost_search_lock = true;
                $.ajax({
                    type: "POST",
                    url: urls.metrost_search,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    headers: { "X-CSRFToken": CSRFToken },
                    data: JSON.stringify({ "country" : $("#meal_country").val(), "city": $("#meal_city").val() }),
                    success: function(data, textStatus, jqXHR) {
                        meal_metrost_search_array = [];
                        for (let search_element_number in data.stations) {
                            if(!data.stations.hasOwnProperty(search_element_number)) continue;
                            meal_metrost_search_array.push({ "name": data.stations[search_element_number].metro_station_en });
                        }
                        meal_metrost_fuse_search = new Fuse(meal_metrost_search_array, fuse_options);
                        meal_metrost_search_lock = false;
                        if (meal_metrost_search_upordown) {
                            let event = jQuery.Event("keydown");
                            event.keyCode = meal_metrost_search_upordown_event;
                            meal_metrost_search_upordown = false;
                            $("#meal_metrost_search").trigger(event);
                        } else {
                            $("#meal_metrost_search").keyup();
                        }                     
                    }
                });
            }
        }
    });
    $("#meal_metrost_search").addClear({ symbolClass: "glyphicon glyphicon-remove", clearOnEscape: false, onClear: function() { $("#meal_metrost_search_results").html(""); $("#meal_metrost_search").change(); }});
    $("#meal_metrost_search").keyup(function(e) {
        if ((e.keyCode != 13) && (e.keyCode != 27) && (e.keyCode != 38 && e.keyCode != 40) && (meal_metrost_search_lock == false))
        {
            let search_filed_value = $(this).val();     // Добываем значения поля поиска
            let search_result = meal_metrost_fuse_search.search(search_filed_value);
            let search_result_html = "";                // HTML для вывода найденных аллергенов

            // Формируем HTML для вывода
            if (search_result != null && search_result.length > 0)    // Проверяем, получили ли мы вообще хоть какой-то список
            { 
                for (let search_result_number in search_result)  // Добываем элементы из списка и формируем HTML подсказки
                {
                    if(!search_result.hasOwnProperty(search_result_number)) continue;
                    search_result_html += search_result_template.replace(/\?search_result/gi, search_result[search_result_number].name);
                }
            }
            $("#meal_metrost_search_results").html(search_result_html);    // Добавляем подготовленную разметку на страницу
            $("#meal_metrost_search_results > .search_result_element").on("click touchend", function(e) { // Добавляем обработчик нажатия на элемент из списка подсказок. Различаем click и touch нажатия.
                $("#meal_metrost_search").val($(this).text());
                $("#meal_metrost_search_results").html("");
                return false;   // Это нужно для игнорирования стандартного действия по нажатию на ссылку (<a>)
            });
        } else if (e.keyCode == 27) {
            $("#meal_metrost_search_results").html("");
        }
    });
    // Обработка нажатия Enter в поле поиска аллергенов
    $("#meal_metrost_search").keypress(function(e) {
        if(e.keyCode == 13)
        {
            let allergen_name = $(this).val();   // Получаем значения поля поиска
            if (allergen_name != null && allergen_name.length > 0)  // Если поле не пустое, обрабатываем его
            {
                if ($('#meal_metrost_search_results').children() != null && $('#meal_metrost_search_results').children().length > 0)
                {   // Если живой поеск нам что-то подсказывает, выбираем по нажатию на Enter первую из подсказок. Так же, можно выбирать элементы через Tab
                    $(this).val($('#meal_metrost_search_results').children().first().text());
                }
                $("#meal_metrost_search_results").html("");
            }
            else    // Если поле пустое, сообщаем пользователю что он не прав
            {
                console.log("Please choose your allergens!");
            }
            e.preventDefault(); // Предотвращаем отправку формы по нажатию на enter
        }
    });
    // Обработка событий нажатия стрелок вниз и вверх для поля поиска
    $("#meal_metrost_search").keydown(function(e) {
        if (e.keyCode == 38 || e.keyCode == 40)
        {
            if (meal_metrost_search_lock == false)
            {
                if ($(this).val() == null || $(this).val() == "")
                {
                    let search_result_html = "";
                    if (meal_metrost_search_array != null && meal_metrost_search_array.length > 0)    // Проверяем, получили ли мы вообще хоть какой-то список
                    { 
                        for (let search_result_number in meal_metrost_search_array)  // Добываем элементы из списка и формируем HTML подсказки
                        {
                            if(!meal_metrost_search_array.hasOwnProperty(search_result_number)) continue;
                            // if(meal_metrost_search_array[search_result_number].lang != "en") continue;
                            search_result_html += search_result_template.replace(/\?search_result/gi, meal_metrost_search_array[search_result_number].name);
                        }
                    }
                    $("#meal_metrost_search_results").html(search_result_html);    // Добавляем подготовленную разметку на страницу
                    $("#meal_metrost_search_results > .search_result_element").on("click touchend", function(e) { // Добавляем обработчик нажатия на элемент из списка подсказок. Различаем click и touch нажатия.
                        $("#meal_metrost_search").val($(this).text());
                        $("#meal_metrost_search_results").html("");
                        return false;   // Это нужно для игнорирования стандартного действия по нажатию на ссылку (<a>)
                    });
                }
                switch(e.keyCode) {
                    case 38:    // Up
                        if ($("#meal_metrost_search_results").children() != null && $("#meal_metrost_search_results").children().length > 0 ) { $("#meal_metrost_search_results").children().last().focus(); }
                        e.preventDefault();
                        break;
                    case 40:    // Down
                        if ($("#meal_metrost_search_results").children() != null && $("#meal_metrost_search_results").children().length > 0 ) { $("#meal_metrost_search_results").children().first().focus(); }
                        e.preventDefault();
                        break;
                }
            }
            else
            {
                meal_metrost_search_upordown = true;
                meal_metrost_search_upordown_event = e.keyCode;
            }
        } else {
            meal_metrost_search_upordown = false;
        }
    });
    $("#meal_metrost_search_results").keydown(function(e) {
        switch(e.keyCode) {
            case 38:    // Up
                if ($(':focus').prev() != null && $(':focus').prev().length > 0) { $(':focus').prev().focus(); } else { $("#meal_metrost_search").focus(); }
                e.preventDefault();
                break;
            case 40:    // Down
                if ($(':focus').next() != null && $(':focus').next().length > 0) { $(':focus').next().focus(); } else { $("#meal_metrost_search").focus(); }       
                e.preventDefault();
                break;
        }
    });
    $("#meal_metrost_search_results").keyup(function(e){
        if (e.keyCode == 27) {
            $("#meal_metrost_search").focus();
            $("#meal_metrost_search_results").html("");
        }
    });
    $("#meal_metrost_search").focusin(function(){
        if ($(this).val() != null && $(this).val().length > 0) {
            $("#meal_metrost_search").trigger(jQuery.Event("keyup"));
        }
    });
    $("#meal_metrost_search").focusout(function() {
        setTimeout(function() {
            if ($("#meal_metrost_search_results").find(":focus").length == 0)
            {
                $("#meal_metrost_search_results").html("");
            }
        }, 10);
    });
    $("#meal_metrost_search_results").focusout(function() {
        var $this = $(this);
        setTimeout(function() {
            if (($this.find(":focus").length == 0) && (!$("#meal_metrost_search").is(":focus")))
            {
                $("#meal_metrost_search_results").html("");
            }
        }, 10);
    });

    // Живой поиск для Meals > Place
    $("#meal_place_search").focusin(function(){
        const city = $("#meal_city").val();
        const street = $("#meal_street_search").val();
        const metrost = $("#meal_metrost_search").val();
        if (city != city_old || street != street_old || metrost != metrost_old)
        {
            // if (((street != null && street.length > 0) || (metrost != null && metrost.length > 0)) || (city != null && city.length == 0))      
            city_old = city;
            street_old = street;
            metrost_old = metrost;
            meal_place_search_lock = true;
            $.ajax({
                type: "POST",
                url: urls.place_search,
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                headers: { "X-CSRFToken": CSRFToken },
                data: JSON.stringify({ "country" : $("#meal_country").val(), "city": city, "street": street, "metrost": metrost }),
                success: function(data, textStatus, jqXHR) {
                    meal_place_search_array = [];
                    for (let search_element_number in data.places) {
                        if(!data.places.hasOwnProperty(search_element_number)) continue;
                        meal_place_search_array.push({ "name": data.places[search_element_number].place_name, "id": data.places[search_element_number].id } );
                    }
                    meal_place_fuse_search = new Fuse(meal_place_search_array, fuse_options);
                    meal_place_search_lock = false;
                    if (meal_place_search_upordown) {
                        let event = jQuery.Event("keydown");
                        event.keyCode = meal_place_search_upordown_event;
                        meal_place_search_upordown = true;
                        $("#meal_place_search").trigger(event);
                    } else {
                        $("#meal_place_search").keyup();
                    }
                }
            });
        }
    });
    $("#meal_place_search").addClear({ symbolClass: "glyphicon glyphicon-remove", clearOnEscape: false, onClear: function() { $("#meal_place_search_results").html(""); $("#meal_place_search").change(); }});
    $("#meal_place_search").keyup(function(e) {
        if ((e.keyCode != 13) && (e.keyCode != 27) && (e.keyCode != 38 && e.keyCode != 40) && (meal_place_search_lock == false))
        {
            let search_filed_value = $(this).val();     // Добываем значения поля поиска
            let search_result = meal_place_fuse_search.search(search_filed_value);
            let search_result_html = "";                // HTML для вывода найденных аллергенов

            // Формируем HTML для вывода
            if (search_result != null && search_result.length > 0)    // Проверяем, получили ли мы вообще хоть какой-то список
            { 
                for (let search_result_number in search_result)  // Добываем элементы из списка и формируем HTML подсказки
                {
                    if(!search_result.hasOwnProperty(search_result_number)) continue;
                    search_result_html += search_result_template.replace(/\?search_result/gi, search_result[search_result_number].name);
                }
            }
            $("#meal_place_search_results").html(search_result_html);    // Добавляем подготовленную разметку на страницу
            $("#meal_place_search_results > .search_result_element").on("click touchend", function(e) { // Добавляем обработчик нажатия на элемент из списка подсказок. Различаем click и touch нажатия.
                $("#meal_place_search").val($(this).text());
                $("#meal_place_search_results").html("");
                return false;   // Это нужно для игнорирования стандартного действия по нажатию на ссылку (<a>)
            });
        } else if (e.keyCode == 27) {
            $("#meal_place_search_results").html("");
        }
    });
    // Обработка нажатия Enter в поле поиска аллергенов
    $("#meal_place_search").keypress(function(e) {
        if(e.keyCode == 13)
        {
            let allergen_name = $(this).val();   // Получаем значения поля поиска
            if (allergen_name != null && allergen_name.length > 0)  // Если поле не пустое, обрабатываем его
            {
                if ($('#meal_place_search_results').children() != null && $('#meal_place_search_results').children().length > 0)
                {   // Если живой поеск нам что-то подсказывает, выбираем по нажатию на Enter первую из подсказок. Так же, можно выбирать элементы через Tab
                    $(this).val($('#meal_place_search_results').children().first().text());
                }
                $("#meal_place_search_results").html("");
            }
            else    // Если поле пустое, сообщаем пользователю что он не прав
            {
                console.log("Please choose your allergens!");
            }
            e.preventDefault(); // Предотвращаем отправку формы по нажатию на enter
        }
    });
    // Обработка событий нажатия стрелок вниз и вверх для поля поиска
    $("#meal_place_search").keydown(function(e) {
        if (e.keyCode == 38 || e.keyCode == 40)
        {
            if (meal_place_search_lock == false)
            {
                if ($(this).val() == null || $(this).val() == "")
                {
                    let search_result_html = "";
                    if (meal_place_search_array != null && meal_place_search_array.length > 0)    // Проверяем, получили ли мы вообще хоть какой-то список
                    { 
                        for (let search_result_number in meal_place_search_array)  // Добываем элементы из списка и формируем HTML подсказки
                        {
                            if(!meal_place_search_array.hasOwnProperty(search_result_number)) continue;
                            // if(meal_place_search_array[search_result_number].lang != "en") continue;
                            search_result_html += search_result_template.replace(/\?search_result/gi, meal_place_search_array[search_result_number].name);
                        }
                    }
                    $("#meal_place_search_results").html(search_result_html);    // Добавляем подготовленную разметку на страницу
                    $("#meal_place_search_results > .search_result_element").on("click touchend", function(e) { // Добавляем обработчик нажатия на элемент из списка подсказок. Различаем click и touch нажатия.
                        $("#meal_place_search").val($(this).text());
                        $("#meal_place_search_results").html("");
                        return false;   // Это нужно для игнорирования стандартного действия по нажатию на ссылку (<a>)
                    });
                }
                switch(e.keyCode) {
                    case 38:    // Up
                        if ($("#meal_place_search_results").children() != null && $("#meal_place_search_results").children().length > 0 ) { $("#meal_place_search_results").children().last().focus(); }
                        e.preventDefault();
                        break;
                    case 40:    // Down
                        if ($("#meal_place_search_results").children() != null && $("#meal_place_search_results").children().length > 0 ) { $("#meal_place_search_results").children().first().focus(); }
                        e.preventDefault();
                        break;
                }
            }
            else
            {
                meal_place_search_upordown = true;
                meal_place_search_upordown_event = e.keyCode;
            }
        } else {
            meal_place_search_upordown = false;            
        }
    });
    $("#meal_place_search_results").keydown(function(e) {
        switch(e.keyCode) {
            case 38:    // Up
                if ($(':focus').prev() != null && $(':focus').prev().length > 0) { $(':focus').prev().focus(); } else { $("#meal_place_search").focus(); }
                e.preventDefault();
                break;
            case 40:    // Down
                if ($(':focus').next() != null && $(':focus').next().length > 0) { $(':focus').next().focus(); } else { $("#meal_place_search").focus(); }       
                e.preventDefault();
                break;
        }
    });
    $("#meal_place_search_results").keyup(function(e){
        if (e.keyCode == 27) {
            $("#meal_place_search").focus();
            $("#meal_place_search_results").html("");
        }
    });
    $("#meal_place_search").focusin(function(){
        if ($(this).val() != null && $(this).val().length > 0) {
            $("#meal_place_search").trigger(jQuery.Event("keyup"));
        }
    });
    $("#meal_place_search").focusout(function() {
        setTimeout(function() {
            if ($("#meal_place_search_results").find(":focus").length == 0)
            {
                $("#meal_place_search_results").html("");
            }
        }, 10);
    });
    $("#meal_place_search_results").focusout(function() {
        var $this = $(this);
        setTimeout(function() {
            if (($this.find(":focus").length == 0) && (!$("#meal_place_search").is(":focus")))
            {
                $("#meal_place_search_results").html("");
            }
        }, 10);
    });

    // Живой поиск для Meals > Meal name
    $("#meal_name_search").focusin(function(){
        const place_name = $("#meal_place_search").val();
        if (place_name != place_name_old)
        {    
            place_name_old = place_name;
            let place_id;
            for (let meal_place_number in meal_place_search_array)  // Добываем элементы из списка и формируем HTML подсказки
            {
                if(!meal_place_search_array.hasOwnProperty(meal_place_number)) continue;
                if(meal_place_search_array[meal_place_number].name == place_name) { place_id = meal_place_search_array[meal_place_number].id; break; }
            }
            if (place_name != null && place_name.length != 0)
            {
                meal_name_search_lock = true;
                $.ajax({
                    type: "POST",
                    url: urls.meal_search,
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    headers: { "X-CSRFToken": CSRFToken },
                    data: JSON.stringify({ "place_id" : place_id }),
                    success: function(data, textStatus, jqXHR) {
                        meal_name_search_array = [];
                        for (let search_element_number in data.meals) {
                            if(!data.meals.hasOwnProperty(search_element_number)) continue;
                            meal_name_search_array.push({ "name": data.meals[search_element_number].meal_name, "id": data.meals[search_element_number].id } );
                        }
                        meal_name_fuse_search = new Fuse(meal_name_search_array, fuse_options);
                        meal_name_search_lock = false;
                        if (meal_name_search_upordown) {
                            let event = jQuery.Event("keydown");
                            event.keyCode = meal_name_search_upordown_event;
                            meal_name_search_upordown = false;
                            $("#meal_name_search").trigger(event);
                        } else {
                            $("#meal_name_search").keyup();
                        }
                    }
                });
            }
        }
    });
    $("#meal_name_search").addClear({ symbolClass: "glyphicon glyphicon-remove", clearOnEscape: false, onClear: function() { $("#meal_name_search_results").html(""); $("#meal_name_search").change(); }});
    $("#meal_name_search").keyup(function(e) {
        if ((e.keyCode != 13) && (e.keyCode != 27) && (e.keyCode != 38 && e.keyCode != 40) && (meal_name_search_lock == false))
        {
            let search_filed_value = $(this).val();     // Добываем значения поля поиска
            let search_result = meal_name_fuse_search.search(search_filed_value);
            let search_result_html = "";                // HTML для вывода найденных аллергенов

            // Формируем HTML для вывода
            if (search_result != null && search_result.length > 0)    // Проверяем, получили ли мы вообще хоть какой-то список
            { 
                for (let search_result_number in search_result)  // Добываем элементы из списка и формируем HTML подсказки
                {
                    if(!search_result.hasOwnProperty(search_result_number)) continue;
                    search_result_html += search_result_template.replace(/\?search_result/gi, search_result[search_result_number].name);
                }
            }
            $("#meal_name_search_results").html(search_result_html);    // Добавляем подготовленную разметку на страницу
            $("#meal_name_search_results > .search_result_element").on("click touchend", function(e) { // Добавляем обработчик нажатия на элемент из списка подсказок. Различаем click и touch нажатия.
                $("#meal_name_search").val($(this).text());
                $("#meal_name_search_results").html("");
                return false;   // Это нужно для игнорирования стандартного действия по нажатию на ссылку (<a>)
            });
        } else if (e.keyCode == 27) {
            $("#meal_name_search_results").html("");
        }
    });
    // Обработка нажатия Enter в поле поиска аллергенов
    $("#meal_name_search").keypress(function(e) {
        if(e.keyCode == 13)
        {
            let allergen_name = $(this).val();   // Получаем значения поля поиска
            if (allergen_name != null && allergen_name.length > 0)  // Если поле не пустое, обрабатываем его
            {
                if ($('#meal_name_search_results').children() != null && $('#meal_name_search_results').children().length > 0)
                {   // Если живой поеск нам что-то подсказывает, выбираем по нажатию на Enter первую из подсказок. Так же, можно выбирать элементы через Tab
                    $(this).val($('#meal_name_search_results').children().first().text());
                }
                $("#meal_name_search_results").html("");
            }
            else    // Если поле пустое, сообщаем пользователю что он не прав
            {
                console.log("Please choose your allergens!");
            }
            e.preventDefault(); // Предотвращаем отправку формы по нажатию на enter
        }
    });
    // Обработка событий нажатия стрелок вниз и вверх для поля поиска
    $("#meal_name_search").keydown(function(e) {
        if (e.keyCode == 38 || e.keyCode == 40)
        {
            if (meal_name_search_lock == false)
            {
                if ($(this).val() == null || $(this).val() == "")
                {
                    let search_result_html = "";
                    if (meal_name_search_array != null && meal_name_search_array.length > 0)    // Проверяем, получили ли мы вообще хоть какой-то список
                    { 
                        for (let search_result_number in meal_name_search_array)  // Добываем элементы из списка и формируем HTML подсказки
                        {
                            if(!meal_name_search_array.hasOwnProperty(search_result_number)) continue;
                            // if(meal_name_search_array[search_result_number].lang != "en") continue;
                            search_result_html += search_result_template.replace(/\?search_result/gi, meal_name_search_array[search_result_number].name);
                        }
                    }
                    $("#meal_name_search_results").html(search_result_html);    // Добавляем подготовленную разметку на страницу
                    $("#meal_name_search_results > .search_result_element").on("click touchend", function(e) { // Добавляем обработчик нажатия на элемент из списка подсказок. Различаем click и touch нажатия.
                        $("#meal_name_search").val($(this).text());
                        $("#meal_name_search_results").html("");
                        return false;   // Это нужно для игнорирования стандартного действия по нажатию на ссылку (<a>)
                    });
                }
                switch(e.keyCode) {
                    case 38:    // Up
                        if ($("#meal_name_search_results").children() != null && $("#meal_name_search_results").children().length > 0 ) { $("#meal_name_search_results").children().last().focus(); }
                        e.preventDefault();
                        break;
                    case 40:    // Down
                        if ($("#meal_name_search_results").children() != null && $("#meal_name_search_results").children().length > 0 ) { $("#meal_name_search_results").children().first().focus(); }
                        e.preventDefault();
                        break;
                }
            }
            else
            {
                meal_name_search_upordown = true;
                meal_name_search_upordown_event = e.keyCode;
            }
        } else {
            meal_name_search_upordown = false;
        }
    });
    $("#meal_name_search_results").keydown(function(e) {
        switch(e.keyCode) {
            case 38:    // Up
                if ($(':focus').prev() != null && $(':focus').prev().length > 0) { $(':focus').prev().focus(); } else { $("#meal_name_search").focus(); }
                e.preventDefault();
                break;
            case 40:    // Down
                if ($(':focus').next() != null && $(':focus').next().length > 0) { $(':focus').next().focus(); } else { $("#meal_name_search").focus(); }       
                e.preventDefault();
                break;
        }
    });
    $("#meal_name_search_results").keyup(function(e){
        if (e.keyCode == 27) {
            $("#meal_name_search").focus();
            $("#meal_name_search_results").html("");
        }
    });
    $("#meal_name_search").focusin(function(){
        if ($(this).val() != null && $(this).val().length > 0) {
            $("#meal_name_search").trigger(jQuery.Event("keyup"));
        }
    });
    $("#meal_name_search").focusout(function() {
        setTimeout(function() {
            if ($("#meal_name_search_results").find(":focus").length == 0)
            {
                $("#meal_name_search_results").html("");
            }
        }, 10);
    });
    $("#meal_name_search_results").focusout(function() {
        var $this = $(this);
        setTimeout(function() {
            if (($this.find(":focus").length == 0) && (!$("#meal_name_search").is(":focus")))
            {
                $("#meal_name_search_results").html("");
            }
        }, 10);
    });

    $("#meal_country").change(function(){
        $("#meal_street_search").val("");
        $("#meal_metrost_search").val("");
        $("#meal_place_search").val("");
        $("#meal_name_search").val("");
    });
    $("#meal_city").change(function(){
        $("#meal_street_search").val("");
        $("#meal_metrost_search").val("");
        $("#meal_place_search").val("");
        $("#meal_name_search").val("");
    });
    $("#meal_street_search").change(function(){
        $("#meal_place_search").val("");
        $("#meal_name_search").val("");
    });
    $("#meal_metrost_search").change(function(){
        $("#meal_place_search").val("");
        $("#meal_name_search").val("");
    });
    $("#meal_place_search").change(function(){
        $("#meal_name_search").val("");
    });

    // Проверка алергена и добавление его в список
    $("#btn_add_allergen").click(function() {
        let allergen_name = $("#allergens_search").val();   // Получаем значение поля поиска на момент нажания кнопки "Добавить"
        let allergen_exists = false;
        let search_result = allergens_fuse_search.search(allergen_name);
        if (search_result != null && search_result.length > 0)    // Если полученный список пуст, значит пользователь ввёл какую-то дичь
        {
            for (let search_result_number in search_result)  // Проходим полученный список аллергенов
            {
                if(!search_result.hasOwnProperty(search_result_number)) continue;
                if (allergen_name == search_result[search_result_number].name)    // И ищем среди них совпадение со значением поля поиска. Если совпадений нет, значит мы не добавляем то что ввёл пользователь
                {
                    if ($.inArray(allergen_name, user_allergens_list_dictionary) == -1)    // Проверяем, а есть ли уже то что ввёл пользователь в списке
                    {   
                        // Готовим данные для сохранения в локальное хранилище и добавления в HTML
                        user_allergens_list_dictionary.push(allergen_name);
                        user_allergens_list_html += user_allergens_list_element_template.replace(/\?allergen_name/gi, allergen_name);
                        // Сохраняем список в локальное хранилище, скрываем сообщение об отсутствии аллергенов, очищаем поле поиска и добавляем новые элементы в HTML
                        localStorage.setItem("user_allergens", JSON.stringify(user_allergens_list_dictionary));
                        $("#no_allergens_message").hide();
                        $("#allergens_search").val("");
                        $("#allergens_search_results").html("");
                        $("#allergens_list").html(user_allergens_list_html);
                        $("#allergens_list > .allergens_list_element:last").css("opacity", "0").animate({ opacity: "1" }, 250).click(function() { return false; });
                        addAlergenRemoveEvents();    // Добавляем обработчики событий на удаление элементов
                    }
                    else    // Иначе, сообщаем, что элемент в списке уже есть
                    {
                        console.log("Allergen " + allergen_name + " already exists");
                    }
                    allergen_exists = true;
                    break;  // Выходим из цикла, так как если мы добавили аллерген, делать в нём нам больше нечего
                }
            }
        }
        if (!allergen_exists)   // Если мы не нашли в списке нужный аллерген, сообщаем пользователю что то что он ввёл - чепуха
        {
            console.log("Allergen " + allergen_name + " does not exists");
        }
    });

    // Проверка продукта, указанного пользователем
    $("#btn_check_product").click(function() {
        let mode = $("#meals_tab_header").hasClass("active") ? "meal" : $("#product_tab_header").hasClass("active") ? "product" : $("#places_tab_header").hasClass("active") ? "places" : "unknown";
        let request_JSON = { request_type: "undefined", allergens: [] };
        switch(mode) {
            case "meal":
                let meal_id;
                const meal_name = $("#meal_name_search").val();
                for (let meal_name_number in meal_name_search_array)  // Добываем элементы из списка и формируем HTML подсказки
                {
                    if(!meal_name_search_array.hasOwnProperty(meal_name_number)) continue;
                    if(meal_name_search_array[meal_name_number].name == meal_name) { meal_id = meal_name_search_array[meal_name_number].id; break; }
                }
                request_JSON = {
                    request_type: mode,
                    meal_id: meal_id,
                    allergens: [],
                    preferences: []
                };
                if (meal_id == null || meal_id.length == 0) {
                    console.log("Cant send information. Meal is not selected"); // Случай, когда одно или несколько полей не заполнены - нужно сказать юзеру                    
                    return false;
                }
                break;
            case "product":
                request_JSON = {
                    request_type: mode,
                    product_name: "",
                    product_manufacturer: "",
                    product_country: "",
                    allergens: [],
                    preferences: []
                };
                request_JSON.product_name = $("#product_name").val();
                request_JSON.product_manufacturer = $("#product_manufacturer").val();
                request_JSON.product_country = $("#product_country").val();
                request_JSON.preferences.push({ is_kosher:$("#is_kosher").is(":checked"), is_halal:$("#is_halal").is(":checked") });
                if (!(request_JSON.product_name && request_JSON.product_manufacturer && request_JSON.allergens))
                {
                    console.log("Cant send information. Some necessary data is missing"); // Случай, когда одно или несколько полей не заполнены - нужно сказать юзеру
                    return false;
                }
                break;
            case "places":
                return false;
            default:
                console.log('Unknown tab used');
                return false;
        }
        for (user_allergen_number in user_allergens_list_dictionary)
        {
            if(!user_allergens_list_dictionary.hasOwnProperty(user_allergen_number)) continue;
            for (db_allergen_number in database.allergens)
            {
                if(!database.allergens.hasOwnProperty(db_allergen_number)) continue;
                if (database.allergens[db_allergen_number].name == user_allergens_list_dictionary[user_allergen_number])
                {
                    request_JSON.allergens.push(database.allergens[db_allergen_number].id);
                }
            }
        }
        $.ajax({
            type: "POST",
            url: urls.check_product,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            headers: { "X-CSRFToken": CSRFToken },
            data: JSON.stringify(request_JSON),
            success: function(data, textStatus, jqXHR) {
                switch(mode) {
                    case "meal":
                        data["product_name"] = $("#meal_name_search").val();
                        break;
                    case "product":
                        data["product_name"] = request_JSON.product_name;
                        break;
                    case "places":
                        break;
                }
                
                let no_checkup_in_history = true;
                for (checkup_number in checkup_history["entries"])
                {
                    if(!checkup_history["entries"].hasOwnProperty(checkup_number)) continue;
                    if (checkup_history["entries"][checkup_number].mid == data.mid)
                    {
                        no_checkup_in_history = false;
                        break;
                    }
                }
                if (no_checkup_in_history) {
                    checkup_history["entries"].push(data);
                    localStorage.setItem('checkup_history', JSON.stringify(checkup_history));
                    let checkup_history_html = $("#checkup_list").html();
                    let checkup_number_i = checkup_history["entries"].length - 1;
                    checkup_history_html += checkup_histroy_element_template.replace(/\?checkup_text/gi, checkup_history["entries"][checkup_number_i].product_name + "<span class='checkup_history_element_status " + (checkup_history["entries"][checkup_number_i].safe ? 'result_text_ok' : 'result_text_danger') + "'>" + checkup_history["entries"][checkup_number_i].result + "</span>").replace(/\?onclick/gi,
                    "displayCheckupResult(checkup_history[\"entries\"][" + checkup_number_i + "])");
                    // $("#no_history_message").hide();
                    $("#checkup_list").html(checkup_history_html);
                }
                displayCheckupResult(data);
                $("#meal_name_search").val("");
                $("#meal_name_search").change("");                
            }
        });
    });

    $("#btn_clear_history").click(function() {
        resetCheckupHistory();
    });

    $(".top_allergen").click(function() {
        $("#allergens_search").val($(this).text());
        $("#btn_add_allergen").click();
    });

    $("#result_close").click(function() { $("body").removeClass("modal-open"); result_modal.style.display = "none"; });
    $(document).click(function(event) { if (event.target == result_modal) { $("body").removeClass("modal-open"); result_modal.style.display = "none"; } });

    $("#meal_city").change(function() {
        if ($(this).val() == null || $(this).val().length == 0) {
            if ($("#street_or_metrost_row").css("display") != "none")
            {
                $("#street_or_metrost_row").css({ opacity: "1", height: "100%" }).animate({ opacity: "0", height: "0" }, 250, function() { $(this).css({ display: "none" })});
            }
        } else {
            if ($("#street_or_metrost_row").css("display") == "none")
            {
                $("#street_or_metrost_row").css({ display: "block", opacity: "0", height: "0" }).animate({ opacity: "1", height: street_or_metrost_row_original_height }, 250);
            }
        }
    });

    $("#meal_country").change(function() {
        $.ajax({
            type: "POST",
            url: urls.get_cities_list,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            headers: { "X-CSRFToken": CSRFToken },
            data: JSON.stringify({ "country" : $(this).val() }),
            success: function(data, textStatus, jqXHR) {
                const cities_HTML_template = "<option value=?city_id>?city_name</option>";
                let cities_HTML = "";
                for (city_number in data.cities)
                {
                    if(!data.cities.hasOwnProperty(city_number)) continue;
                    cities_HTML += cities_HTML_template.replace(/\?city_id/gi, data.cities[city_number].city_or_town_en).replace(/\?city_name/gi, data.cities[city_number].city_or_town_en);
                }
                cities_HTML += "<option value='' selected>Not in list / General</option>";
                $("#meal_city").html(cities_HTML);
                $("#meal_city").change();   
            }
        });
    });
    $("#meal_country").change();


    $(document).keyup(function(e){
        if($("body").hasClass("modal-open") && e.keyCode == 27)
        {
            $("body").removeClass("modal-open");
            result_modal.style.display = "none";
        }
    });

    $('[data-toggle="tooltip"]').tooltip();
});

// Функция загрузки и обновления базы данных с полным списком аллергенов
function loadDatabase()
{
    // Получение проверочной суммы из хранилища пользователя. Если там пусто - используем нулевое значение.
    let database_JSON = localStorage.getItem("database");
    let database_checksum = "0";

    if (database_JSON != null && database_JSON.length > 0)
    {
        database = JSON.parse(database_JSON);
        if (database.checksum != null && database.checksum.length > 0) { database_checksum = database.checksum; }
    }

    // Проверка базы данных на необходимость обновления по проверочной сумме
    allergens_search_lock = true;
    $.ajax({
        type: "POST",
        url: urls.allergens_db,
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        headers: { "X-CSRFToken": CSRFToken },
        data: JSON.stringify({ "checksum": database_checksum }),
        success: function(data, textStatus, jqXHR) {
            if (data.checksum != null && data.checksum.length > 0)
            {
                if (database_checksum != data.checksum)   // Если checksum не совпадает, то база либо отсутсвует, либо устаревшая. Обновляем.
                {
                    localStorage.setItem("database", JSON.stringify(data));
                    database = JSON.parse(localStorage.getItem("database"));
                    allergens_fuse_search = new Fuse(database.allergens, fuse_options);   // Индексирование для поиска или что оно там
                    console.log("База данных успешно обновлена");
                }
                else
                {
                    console.log("База данных не нуждается в обновлении");
                }
                allergens_loaded = true;
                allergens_search_lock = false;
                if (allergens_search_upordown) {
                    let event = jQuery.Event("keydown");
                    event.keyCode = allergens_search_upordown_event;
                    allergens_search_upordown = false;
                    $("#allergens_search").trigger(event);
                } else {
                    $("#allergens_search").keyup();
                }
            }
            else { showDatabaseUpdateErrorMessage(); }
        },
        error: function(jqXHR, textStatus, errorThrown) { showDatabaseUpdateErrorMessage(); }
    });
    
    allergens_fuse_search = new Fuse(database.allergens, fuse_options);   // Индексирование для поиска или что оно там
}

// Функция, отображающее оповещение об ошибке обновления БД.
function showDatabaseUpdateErrorMessage()
{
    console.log("Ошибка при обновлении базы данных");
}

// Функция, добавляющая обработчик для удаления элементов из списка подсказок
function addAlergenRemoveEvents()
{
    // Функция, обрабатывающая нажатия на "Х" в списке аллергенов.
    $("#allergens_list > .allergens_list_element > span.glyphicon").click(function() {
        user_allergens_list_dictionary.splice(user_allergens_list_dictionary.indexOf($(this).parent().text()), 1);    // Удаляем аллерген из списка аллергенов в памяти
        localStorage.setItem("user_allergens", JSON.stringify(user_allergens_list_dictionary)); // Удаляем аллерген из локального хранилища
        user_allergens_list_html = $("#allergens_list").html().replace($(this).parent().clone().wrap("<p>").parent().html(), "");  // Удаляем аллерген из хранимого в памяти HTML
        $(this).parent().animate({ opacity: "0" }, 250, function() { 
            $(this).remove(); // Удаялем аллерген из HTML.
            if(user_allergens_list_dictionary.length == 0) { $("#no_allergens_message").show(); }  // Если был удалён последний аллерген, отображаем сообщение об отсутсвии аллергенов в списке
        });
    });
    // Функция, обрабатыващая нажатия на стрелки и клавиши Del, Backspace, когда пользователь работает со списком аллергенов 
    $("#allergens_list > .allergens_list_element").keydown(function(e) { 
        switch(e.keyCode) {
            case 46:    // Delete
            case 8:     // Backspace
                if ($(this).next() != null && $(this).next().length > 0)
                {
                    $(this).next().focus();
                } else if ($(this).prev() != null && $(this).prev().length > 0) {
                    $(this).prev().focus();
                }
                $(this).children().last().click();
                break;
            case 37:    // Left
            case 38:    // Up
                if ($(this).prev() != null && $(this).prev().length > 0) { $(this).prev().focus(); }
                break;
            case 39:    // Right
            case 40:    // Down
                if ($(this).next() != null && $(this).next().length > 0) { $(this).next().focus(); }
                break;
        }
    });
}

// Отображение модального окна с результататми проверки продукта
function displayCheckupResult(data){
    $("#result_header").parent().removeClass("result_green").removeClass("result_red");
    $("#result_header").parent().addClass(data.safe ? "result_ok" : "result_danger");
    document.getElementById('result_header').innerHTML = data.product_name + " is " + data.result + " for you";
    let result_HTML = "";
    // result_HTML += "<h4>Confidence: " + data.confidence + "</h4>";
    result_HTML += "<h4>Allergens checked:</h4>";
    let allergen_number = 0;
    for (allergen_number in data.allergens)
    {
        if(!data.allergens.hasOwnProperty(allergen_number)) continue;
        for (db_allergen_number in database.allergens)
        {
            if(!database.allergens.hasOwnProperty(db_allergen_number)) continue;
            if ((database.allergens[db_allergen_number].id == data.allergens[allergen_number].id) && database.allergens[db_allergen_number].lang == "en")
            {
                result_HTML += modal_collapsable_template.replace(/\?collapse_id/gi, allergen_number).replace(/\?collapse_heading/gi, database.allergens[db_allergen_number].name + "<span class='checkup_history_allergen_status " + (data.allergens[allergen_number].status == "OK" ? 'result_text_ok' : (data.allergens[allergen_number].status == "DANGER" ? 'result_text_danger' : 'result_text_warning')) + "'>" + data.allergens[allergen_number].status + "</span>").replace(/\?collapse_body/gi, 
                "<p>Official Information: " + (data.allergens[allergen_number].listed_by_manufacturer == true ? (data.allergens[allergen_number].manufacturer_info == 0 ? "Contains " + database.allergens[db_allergen_number].name : (data.allergens[allergen_number].manufacturer_info == 1 ? "May contain " + database.allergens[db_allergen_number].name : "Does not contain " + database.allergens[db_allergen_number].name)) : "Not available") + "</p>" + 
                "<p>Societies information: " + (data.allergens[allergen_number].checked_by_societies == true ? (data.allergens[allergen_number].societies_info == 0 ? "Contains " + database.allergens[db_allergen_number].name : (data.allergens[allergen_number].societies_info == 1 ? "May contain " + database.allergens[db_allergen_number].name : "Does not contain " + database.allergens[db_allergen_number].name)) : "Not available") + "</p>" +
                "<p>Considered safe: " + (data.allergens[allergen_number].considered_safe == true ? "Yes" : "No") + "</p>" +
                "<p>User reports: " + data.allergens[allergen_number].reports_by_users + "</p>");
                break;
            }
        }
    }
    
    result_HTML += modal_collapsable_template.replace(/\?collapse_id/gi, allergen_number + 1).replace(/\?collapse_heading/gi, "Nutrition Info").replace(/\?collapse_body/gi, 
    "<p>Protein: " + (data.facts[0].protein == null ? "Unknown" : data.facts[0].protein) + "</p>" +
    "<p>Fat: " + (data.facts[0].fat == null ? "Unknown" : data.facts[0].fat) + "</p>" + 
    "<p>Carbohydrates: " + (data.facts[0].carbo == null ? "Unknown" : data.facts[0].carbo) + "</p>" +
    "<p>Calories: " + (data.facts[0].calories == null ? "Unknown" : data.facts[0].calories) + "</p>");
    result_HTML += "<button type='button' class='btn btn-success btn_close_checkup_results' id='btn_close_checkup_results' onclick='document.getElementById(\"result_window\").style.display = \"none\"; $(\"body\").removeClass(\"modal-open\");'>Close</button></div>";
    document.getElementById('result_body').innerHTML = result_HTML;
    $("body").addClass("modal-open");
    result_modal.style.display = "block";
}