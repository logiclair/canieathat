#!/usr/bin/env python
# -*- coding:utf-8 -*-

from .models import Country, Meal, Place

def evaluate_meal(meal, allergens_list):
	result = {}
	
	result["mid"] = "meal" + str(meal.id)
	allergs = [] 
	status = 0
	for allergen in allergens_list:
		aldict = {}
		# идентификатор аллергена
		aldict["id"] = allergen
		ind = int(allergen)
		# Если значение этой переменной равно 1, значит производитель указал информаци об аллергене
		aldict["listed_by_manufacturer"] = meal.listed_by_manufacturer[ind]=="1"
		if aldict["listed_by_manufacturer"]:
			aldict["manufacturer_info"] = int(meal.manufacturer_info[ind])
		else:
			aldict["manufacturer_info"] = 0
			# 1 значит, что информация проверена сообществом
		aldict["checked_by_societies"] = meal.checked_by_societies[ind]=="1"
		if aldict["checked_by_societies"]:
			aldict["societies_info"] = int(meal.societies_info[ind])
		else:
			aldict["checked_by_societies"] = 0

		# Проверяем, были ли репорты от юзеров и сколько
		if meal.reports_unsafe: 
			aldict["reports_by_users"] = int(meal.reports_unsafe.split(';')[ind])
		else: 
			aldict["reports_by_users"] = 0

		# Основная логика
		# Если репортов > 3: 
		if aldict["reports_by_users"] > 0:
			aldict["status"] = "DANGER"
			aldict["considered_safe"] = False
			status +=1
		# Если меньше 3 или репортов нет
		else:
			# Если есть информация от сообществ: 
			if aldict["checked_by_societies"]:
				# Если точно нет:
				if aldict["societies_info"] == 2:
					aldict["status"] = "DEFINITELY SAFE"
					aldict["considered_safe"] = True
				# Если могут быть следы
				elif aldict["societies_info"] == 1:
					aldict["status"] = "WARNING"
					aldict["considered_safe"] = False
					status +=1
				# Если точно есть
				else:
					aldict["status"] = "DANGER"
					aldict["considered_safe"] = False
					status +=1
			# Если нет информации от сообществ
			else:
				if aldict["listed_by_manufacturer"]:
					# Если точно нет
					if aldict["manufacturer_info"] == 2:
						aldict["status"] = "SAFE"
						aldict["considered_safe"] = True
					# Если следы
					elif aldict["manufacturer_info"] == 1:
						aldict["status"] = "WARNING"
						aldict["considered_safe"] = False
						status +=1
					else:
						aldict["status"] = "DANGER"
						aldict["considered_safe"] = False
						status +=1
				else:
					aldict["status"] = "SUSPICIOUS"
					aldict["considered_safe"] = False
					status +=1

		allergs.append(aldict)
	result["allergens"] = allergs
	facts = {}
	facts["fat"] = meal.fat
	facts["protein"] = meal.protein
	facts["carbo"] = meal.carbohydrates
	facts["calories"] = meal.calories
	result["facts"] = [facts]
	result["safe"] = status == 0
	# TODO: provide metric
	result["confidence"] = "High"
	if result["safe"]:
		result["result"] = "safe"
	else:
		result["result"] = "unsafe"
	return result