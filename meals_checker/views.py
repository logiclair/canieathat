#!/usr/bin/env python
# -*- coding:utf-8 -*-

# Django imports
from django.views.generic import View
from django.shortcuts import render, render_to_response
from django.http import HttpResponse
from django.template import RequestContext
from django.core import serializers
from django.db.models import Q

# Python imports
import json, hashlib
import os.path

# Local imports
from .models import Country, Meal, Place
from .evaluators import evaluate_meal


ALLERGENS_FILENAME_REL = 'static/assets/allergens.json'
ALLERGENS_FILENAME = os.path.join(os.path.dirname(__file__), 
    ALLERGENS_FILENAME_REL)

SAMPLE_RESULT_FILENAME_REL = 'static/assets/sample_result.json'
SAMPLE_RESULT_FILENAME = os.path.join(os.path.dirname(__file__), 
    SAMPLE_RESULT_FILENAME_REL)

# Constants

with open(ALLERGENS_FILENAME, encoding="utf-8") as allergens_json:
    ALLERGENS = json.load(allergens_json)
    ALLERGENS["checksum"] = str(hashlib.md5(json.dumps(ALLERGENS, ensure_ascii = False).encode("utf-8")).hexdigest())    
with open(SAMPLE_RESULT_FILENAME) as sample_json:
    TEST_QUERY_RESULTS = json.load(sample_json)



class Index(View):

    # TODO    Здесь нужно добавить вывод в контекст, необходимый для формирования списка стран в index.html
    def get(self, request, *args, **kwargs):

        context_dict = {}
        context = RequestContext(request)

        # get all countries
        countries = list(map(lambda x: (x.country_code, x.country_name, x.country_name_en), Country.objects.all()))
        
        context_dict["countries"] = countries

        return render(request,
                    "pages/index.html",
                    context_dict)

    def post(self, request, *args, **kwargs):

        json_request = json.loads(request.body.decode('utf-8'))
        request_type = json_request.get("request_type")
        request_meal = json_request.get("meal_id")
        meal = Meal.objects.get(id = request_meal)
        # line to remove
        # request_meal = 1

        allergens = json_request.get("allergens")
        result = evaluate_meal(meal, allergens)
        
        return HttpResponse(json.dumps(result, ensure_ascii = False).encode("utf-8"), content_type="application/json")
"""
{
    "safe": "1",
    "confidence": "High",
    "result": "Safe",

"""
# Функция получения списка городов по стране
class CitiesByCountry(View):

    def post(self, request, *args, **kwargs):
    
        json_request = json.loads(request.body.decode('utf-8'))
    
        request_country = json_request.get("country")       # Получаем из POST запроса страну
        # request_country = 1
        cities = list(Place.objects.
            filter(country = request_country).
            exclude(city_or_town_en__isnull = True).
            values('city_or_town_en').
            distinct())
        cities_json = "{\"cities\": " + json.dumps(cities, ensure_ascii = False) + "}"
        """
        # В зависимости от страны формируем список городов и возвращаем его
        response = { "cities": [
            { "name" : "Milton Keynes" },
            { "name" : "Bedford" },
            { "name" : "London" }
        ]}
        """
        return HttpResponse(cities_json.encode("utf-8"), content_type="application/json")

class StreetsByCity(View):

    def post(self, request, *args, **kwargs):
        
        json_request = json.loads(request.body.decode('utf-8'))
        request_country = json_request.get("country")           
        request_city = json_request.get("city")      

        # remove the folowing to lines when ready to test real search
        # request_country = 1
        # request_city = "Milton Keynes"

        streets = list(Place.objects.
            filter(country = request_country).
            filter(city_or_town_en = request_city).
            exclude(street_name_en__isnull = True).
            values('street_name_en').
            distinct())
        streets_json = "{\"streets\":" + json.dumps(streets, ensure_ascii = False) + "}"
        # В зависимости от страны и города (потому что бывают города с одинаковым названием) выполняем поиск улиц
        #         Важно!    В реальном случае будут возвращатся улицы с одинаковым началом названия или похожими названиями.
        #                   Это просто пример, потому улицы какие-попало.
        #                   Если ничего не найдено, возвращается пустой список { search_results":[] }
        """
        response = { 
        "search_results": [
        { "name" : "Olimpiiska" },
        { "name" : "Bakulina" },
        { "name" : "Serpova" },
        { "name" : "Danylenko" }
        ]}
        """
        return HttpResponse(streets_json.encode("utf-8"), content_type="application/json")


class TubeStationByCity(View):

    def post(self, request, *args, **kwargs):
        
        json_request = json.loads(request.body.decode('utf-8'))
        request_country = json_request.get("country")           # Получаем из POST запроса страну
        request_city = json_request.get("city")             # Получаем из POST запроса город
        
        # В зависимости от страны и города выполняем поиск станций метро
        #         Важно!    В реальном случае будут возвращатся станции мето с одинаковым началом названия или похожими названиями.
        #                   Это просто пример, потому станции взяты случайные.
        #                   Если ничего не найдено, возвращается пустой список { search_results":[] }
        # TODO    Возможно, стоит сделать не поиск по названию станции,
        #         а два поля выбора, если метро в выбранном городе есть - сначала линию, потом станцию.
        # request_country = 0
        # request_city = "Kharkiv"
        stations = list(Place.objects.
            filter(country = request_country).
            filter(city_or_town_en = request_city).
            exclude(metro_station_en__isnull = True).
            values('metro_station_en').
            distinct())
        stations_json = "{\"stations\":" + json.dumps(stations, ensure_ascii = False) + "}"
        """
        response = { "search_results": [
            { "name" : "Naukova" },
            { "name" : "Universytet" },
            { "name" : "Puskinska" },
            { "name" : "Kyivska" }
        ]}
        """
        return HttpResponse(stations_json.encode("utf-8"), content_type="application/json")

# Функция получения списка заведений по поиску, в зависимости от страны, города и улицы/станциии метро
class PlacesByAddress(View):

    def post(self, request, *args, **kwargs):
        
        json_request = json.loads(request.body.decode('utf-8'))
        request_country = json_request.get("country")        
        request_city = json_request.get("city")             
        request_street = json_request.get("street")         
        request_metro = json_request.get("metrost")  
        # request_city = ""
        # request_metro = ""
        # request_street = ""  
        places = Place.objects.filter(country = request_country)
        if len(request_city) == 0 and len(request_street) == 0 and len(request_metro) == 0:
            places = places.filter(city_or_town_en__isnull = True)
        elif len(request_street) == 0 and len(request_metro) == 0:
            places = (places.
                filter(city_or_town_en = request_city).
                filter(Q(street_name_en__isnull = True) & Q(metro_station_en__isnull = True)))
        else: 
            places = (places.
                filter(city_or_town_en = request_city).
                filter(Q(street_name_en = request_street) | Q(metro_station_en = request_metro)))
        places = list(places.values('id', 'place_name', 'place_common_name').distinct())

        # request_country = 0          
        # request_city = "Kharkiv"     
        # request_street = "Nauki Ave"
        # request_metro = "Naychnaya"
        """
        places = list(Place.objects.
            filter(country = request_country).
            filter(city_or_town_en = request_city).
            filter((Q(street_name_en = request_street) 
                & Q(street_name_en__isnull = False)) | (Q(metro_station_en = request_metro) 
                & Q(metro_station_en__isnull = False))).
            values('id', 'place_name', 'place_common_name').
            distinct())
        """
        places_json = "{\"places\":" + json.dumps(places, ensure_ascii = False) + "}"
        """
        response = { "search_results": [
            { "name" : "PARMA" },
            { "name" : "Mc. Donalds" },
            { "name" : "Lvivska Kavyarnya" }
        ]}
        """
        return HttpResponse(places_json.encode("utf-8"), content_type="application/json")

class MealSearch(View):

    def post(self, request, *args, **kwargs): 
        
        json_request = json.loads(request.body.decode('utf-8'))
        place_id = json_request.get("place_id")
        meals = list(Meal.objects.filter(place = place_id).values('id', 'meal_name'))
        meals_json = "{\"meals\":" + json.dumps(meals, ensure_ascii = False) + "}"
        """
        response = { "search_results": [
            { "name" : "Steak" },
            { "name" : "Soup \"Ministrone\"" },
            { "name" : "Squid" }
        ]}
        """
        return HttpResponse(meals_json.encode("utf-8"), content_type="application/json")


class AllergensDB(View):

    def post(self, request, *args, **kwargs): 
        
        
        json_request = json.loads(request.body.decode('utf-8'))
        #json_request = json.loads(request.body)
        # ALLERGENS = {
        #     "checksum": "0",
        #     "allergens": [ 
        #         {"id": "0", "name": "глютен", "lang": "ru"}, 
        #         {"id": "0", "name": "gluten", "lang": "en"}, 
        #         {"id": "0", "name": "cereals", "lang": "en"}, 
        #         {"id": "0", "name": "wheat", "lang": "en"},
        #         {"id": "1", "name": "crustaceans", "lang": "en"}, 
        #         {"id": "1", "name": "ракообразные", "lang": "ru"}, 
        #         {"id": "2", "name": "яйца","lang": "ru"}, 
        #         {"id": "2", "name": "eggs", "lang": "en"}, 
        #         {"id": "3", "name": "рыба", "lang": "ru"},
        #         {"id": "3", "name": "fish", "lang": "en"},
        #         {"id": "4", "name": "арахис", "lang": "ru"},
        #         {"id": "4", "name": "peanuts", "lang": "en"},
        #         {"id": "5", "name": "соя", "lang": "ru"},
        #         {"id": "5", "name": "soybeans", "lang": "en"},
        #         {"id": "6", "name": "молоко", "lang": "ru"},
        #         {"id": "6", "name": "milk", "lang": "en"},
        #         {"id": "6", "name": "лактоза", "lang": "ru"},
        #         {"id": "6", "name": "lactose", "lang": "en"},
        #         {"id": "7", "name": "сельдерей", "lang": "ru"},
        #         {"id": "7", "name": "celery", "lang": "en"},
        #         {"id": "8", "name": "горчица", "lang": "ru"},
        #         {"id": "8", "name": "mustard", "lang": "en"},
        #         {"id": "9", "name": "кунжут", "lang": "ru"},
        #         {"id": "9", "name": "sesame", "lang": "en"},
        #         {"id": "10", "name": "sulphites", "lang": "en"},
        #         {"id": "10", "name": "сульфиты", "lang": "ru"},
        #         {"id": "11", "name": "nuts", "lang": "en"},
        #         {"id": "11", "name": "орехи", "lang": "ru"},
        #         {"id": "12", "name": "lupin", "lang": "en"},
        #         {"id": "12", "name": "люпин", "lang": "ru"},
        #         {"id": "13", "name": "molluscs", "lang": "en"},
        #         {"id": "13", "name": "моллюски", "lang": "ru"}
        #     ]
        # }
        # ALLERGENS["checksum"] = str(hashlib.md5(json.dumps(ALLERGENS, ensure_ascii = False).encode("utf-8")).hexdigest())            
        if (json_request.get("checksum") == ALLERGENS["checksum"]):
            DB_JSON_Cleaned = dict(ALLERGENS)
            del DB_JSON_Cleaned["allergens"]
            return HttpResponse(json.dumps(DB_JSON_Cleaned, ensure_ascii = False).encode("utf-8"), content_type="application/json")
        return HttpResponse(json.dumps(ALLERGENS, ensure_ascii = False).encode("utf-8"), content_type="application/json")