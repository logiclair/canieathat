from django.apps import AppConfig


class MealsCheckerConfig(AppConfig):
    name = 'meals_checker'
