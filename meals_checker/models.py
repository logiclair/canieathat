# Create your models here.

#!/usr/bin/env python
# -*- coding:utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Country lookup table
class Country(models.Model):
	country_code = models.CharField(max_length = 200, primary_key = True)
	country_name = models.CharField(max_length = 200)
	country_name_en = models.CharField(max_length = 200)

	def __str__(self):
		return self.country_name_en


# Packed product manufactor
class Manufactor(models.Model):
	# id is created automatically

	# official name (with LLC and things like that)
	manufactor_offical_name = models.CharField(max_length = 200, unique = True)
	# name typically used by people
	manufactor_common_name = models.CharField(max_length = 200, null = True, blank = True, default = None)
	# names known by people or in other languages
	alternative_names = models.TextField(null = True, blank = True, default = None)

	# country of the main office of the company 
	country = models.ForeignKey(Country)
	# website address
	website = models.URLField(max_length = 200, null = True, blank = True, default = None)

	# if the manufacturer states allergy info
	is_allergic_aware = models.NullBooleanField(null = True, blank = True, default = None)
	# if the manufacturer states allergy info
	in_contact = models.BooleanField(default = False)
	# contact person
	contact_person_full_name = models.CharField(max_length = 200, null = True, blank = True, default = None)
	contact_person_email = models.EmailField(null = True, blank = True, default = None)
	contact_person_phone = models.CharField(max_length = 200,null = True, blank = True, default = None)

	def __str__(self):
		return self.manufactor_offical_name

class Facility(models.Model):

	manufactor = models.ForeignKey(Manufactor)

	country = models.ForeignKey(Country)

	city = models.CharField(max_length=200, null = True, blank = True, default = None) 
	in_contact = models.NullBooleanField(default = False)

	contact_person_full_name = models.CharField(max_length=200,null = True, blank = True, default = None)
	contact_person_email = models.EmailField(null = True, blank = True, default = None)
	contact_person_phone = models.CharField(max_length=200, null = True, blank = True, default = None)
	# list of products handled, if known
	types_of_products_handles = models.CharField(max_length=500, null = True, blank = True, default = None)

class Place(models.Model):
	# id is created automatically

	# name of the place (e.g. Weatherspoons Queens) -> some local name, can be empty
	place_name = models.CharField(max_length=200, null=True, blank=True, default = None)
	# name used by people (e.g. u"Мак")
	place_common_name = models.CharField(max_length=200,null=True, blank=True, default = None)
	# other anmes (e.g. u"Макдак")
	alternative_names = models.TextField(null = True, blank = True, default = None)
	# url of this particular palce
	website = models.URLField(max_length=200, null = True, blank = True, default = None)
	latitude = models.FloatField(null=True, blank=True, default = None)
	longitude = models.FloatField(null=True, blank=True, default = None)

	country = models.ForeignKey(Country)

	city_or_town = models.CharField(max_length=200, null=True, blank=True, default = None) 
	city_or_town_en = models.CharField(max_length=200, null=True, blank=True, default = None) 
	metro_station = models.CharField(max_length=200, null=True, blank=True, default = None) 
	metro_station_en = models.CharField(max_length=200, null=True, blank=True, default = None) 
	street_name = models.CharField(max_length=200, null=True, blank=True, default = None) 
	street_name_en = models.CharField(max_length=200, null=True, blank=True, default = None) 
	street_number_or_name = models.CharField(max_length=200, null=True, blank=True, default = None)
	street_number_or_name_en = models.CharField(max_length=200, null=True, blank=True, default = None)
	room_or_id = models.CharField(max_length=200, null = True, blank = True, default = None)
	room_or_id_en = models.CharField(max_length=200, null = True, blank = True, default = None)
	postal_code = models.CharField(max_length = 200,null = True, blank = True, default = None)

	#if the data for this place is indivudal or inferred from other places in the chain
	checked_individually = models.BooleanField(default = False)
	is_allergic_aware = models.NullBooleanField(null = True, blank = True, default = None)

	# if the manufacturer states allergy info
	in_contact = models.BooleanField(default = False)

	contact_person_full_name = models.CharField(max_length=200, null = True, blank = True, default = None)
	contact_person_full_name_en = models.CharField(max_length=200, null = True, blank = True, default = None)
	contact_person_email = models.EmailField(null = True, blank = True, default = None)
	contact_person_phone = models.CharField(max_length=200, null = True, blank = True, default = None)

	comment = models.TextField(null = True, blank = True, default = None)

	unique_together = ('place_name','country', 'city_or_town', 'street_name', 'city_or_town', 'street_number_or_name', 'room_or_id')

	def __str__(self):
		if self.city_or_town_en: 
			ud = self.city_or_town_en 
		else: 
			ud = ""
		return self.place_name + " in " + ud

class PackedProduct(models.Model):
	# id is created automatically
	trade_name = models.CharField(max_length=200)
	common_name = models.CharField(max_length=200, null = True, blank = True, default = None)
	alternative_names = models.TextField(null = True, blank = True, default = None)
	manufactor = models.ForeignKey(Manufactor, null = True, blank = True, default = None)
	flavor = models.CharField(max_length=200, null = True, blank = True, default = None)
	# country where the product was actually produced
	country_produced = models.ForeignKey(Country, related_name='country_produced')

	# 01 string of allegrens, claimed by manufactor
	claimed_allergens_string = models.CharField(max_length=500, null = True, blank = True, default = None)

	# 01 string of allegrens, see codebook
	actual_allergens_string = models.CharField(max_length=500, null = True, blank = True, default = None)

	# comma separated list
	reports_unsafe = models.CharField(max_length=1000, null = True, blank = True, default = None)

	# boolean fields
	vegan = models.NullBooleanField(default=False, null = True, blank = True)
	vegan_confidence = models.FloatField(default=0.5, null = True, blank = True)
	vegan_source = models.CharField(max_length=200,null = True, blank = True)
	kosher = models.NullBooleanField(default=False, null = True, blank = True)
	kosher_confidence = models.FloatField(default=0.5,null = True, blank = True)
	kosher_source = models.CharField(max_length=200,null = True, blank = True)
	halal = models.NullBooleanField(default=False,null = True, blank = True)
	halal_confidence = models.FloatField(default=0.5,null = True, blank = True)
	halal_source = models.CharField(max_length=200,null = True, blank = True)
	vegetarian = models.NullBooleanField(default=False,null = True, blank = True)
	vegetarian_confidence = models.FloatField(default=0.5,null = True, blank = True)
	vegetarian_source = models.CharField(max_length=200, null = True, blank = True)
	junk_food = models.NullBooleanField(null = True, blank = True)

	# per 100 gram
	calories = models.FloatField(null = True, blank = True, default = None)
	fat = models.FloatField(null = True, blank = True, default = None)
	carbohydrates = models.FloatField(null = True, blank = True, default = None)
	protein = models.FloatField(null = True, blank = True, default = None)

	unique_together = ("trade_name", "manufactor", "country_produced")

	def __str__(self):
		return self.trade_name


class Meal(models.Model):
	# id is created automatically 
	meal_name = models.CharField(max_length=200)
	alternative_names = models.TextField(null = True, blank = True, default = None)
	place = models.ForeignKey(Place)

	listed_by_manufacturer = models.CharField(max_length=500,null = True, blank = True, default = None)
	manufacturer_info = models.CharField(max_length=500,null = True, blank = True, default = None)
	checked_by_societies = models.CharField(max_length=500,null = True, blank = True, default = None)
	societies_info = models.CharField(max_length=500,null = True, blank = True, default = None)

	# comma separated list
	reports_unsafe = models.CharField(max_length=1000,null = True, blank = True, default = None)

	# boolean fields
	vegan = models.NullBooleanField(default=False,null = True, blank = True)
	vegan_confidence = models.FloatField(default=0.5, null = True, blank = True)
	vegan_source = models.CharField(max_length=200,null = True, blank = True, default = None,)
	kosher = models.NullBooleanField(default=False, null = True, blank = True)
	kosher_confidence = models.FloatField(default=0.5, null = True, blank = True)
	kosher_source = models.CharField(max_length=200,null = True, blank = True)
	halal = models.NullBooleanField(default=False, null = True, blank = True)
	halal_confidence = models.FloatField(default=0.5, null = True, blank = True)
	halal_source = models.CharField(max_length=200,null = True, blank = True)
	vegetarian = models.NullBooleanField(default=False, null = True, blank = True)
	vegetarian_confidence = models.FloatField(default=0.5, null = True, blank = True)
	vegetarian_source = models.CharField(max_length=200,null = True, blank = True)
	junk_food = models.NullBooleanField(null = True, blank = True)

	# per 100 gram
	calories = models.FloatField(null = True, blank = True, default = None)
	fat = models.FloatField(null = True, blank = True, default = None)
	carbohydrates = models.FloatField(null = True, blank = True, default = None)
	protein = models.FloatField(null = True, blank = True, default = None)

	def __str__(self):
		return self.meal_name