from __future__ import unicode_literals


from django.contrib import admin
"""
from meals_checker.models import (Country, ContactPerson, 
	Manufactor, Facility,
	Chain, Place, 
	PackedProduct, Meal)
"""
from meals_checker.models import *
from django.utils.translation import ugettext_lazy as _


class FacilityInline(admin.TabularInline):
    model = Facility

class ManufactorAdmin(admin.ModelAdmin):
    inlines = [
        FacilityInline,
    ]

class PlaceInline(admin.TabularInline):
	model = Place

class MealInline(admin.TabularInline):
	model = Meal

class PlaceAdmin(admin.ModelAdmin):
	inlines = [
		MealInline,
	]
	list_display = (
		'place_name',
		'country',
		'city_or_town'
		)
admin.site.register(Country)
admin.site.register(Manufactor, ManufactorAdmin)
admin.site.register(PackedProduct)
admin.site.register(Meal)
admin.site.register(Place, PlaceAdmin)