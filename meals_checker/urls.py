from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^$', views.Index.as_view(), name='index'),
    url(r'^service/get_allergens_db/$', views.AllergensDB.as_view(), name="get_allergens_db"),
    url(r'^service/check_product/$', views.Index.as_view(), name="check_product"),
    url(r'^service/get_cities_list_by_country/$', views.CitiesByCountry.as_view(), name="get_cities_list_by_country"),
    url(r'^service/street_search/$', views.StreetsByCity.as_view(), name="street_search"),
    url(r'^service/metrostation_search/$', views.TubeStationByCity.as_view(), name="metrostation_search"),
    url(r'^service/place_search/$', views.PlacesByAddress.as_view(), name="place_search"),
    url(r'^service/meal_search/$', views.MealSearch.as_view(), name="meal_search")
    # TODO    Создать обработчики для поиска улиц, станции метро, названий заведений и блюд.
]