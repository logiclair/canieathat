from django.test import TestCase
from .evaluators import evaluate_meal
from .models import Meal, Country, Place
# Create your tests here.


def convert_string(string_to_convert,pos, increment = True):
    a = string_to_convert.split(';')
    if increment:
        a[pos] = int(a[pos])+1
    else:
        a[pos] = int(a[pos])-1
    return ";".join(str(i) for i in a) 

def get_col_sep_list(str_list):
	return ";".join(str(i) for i in a) 

def get_012_string(str_list):
	a = ";".join(str(i) for i in a) 
	return 

class MealEvaulation(TestCase):

    def setUp(self):
    	country = Country.objects.create(
    		country_code = 0,
    		country_name = "Ukraine",
    		country_name_en = "Ukraine")
    	place = Place.objects.create(
    		place_name = "Sushyia",
    		country = country)

    	Meal.objects.create(
    		meal_name = "TestMeal",
    		place = place,
    		listed_by_manufacturer = '00000000010000',
    		manufacturer_info = '00000000000000',
    		checked_by_societies = '00000000000000',
    		societies_info = '00000000000000',
    		reports_unsafe = '0;0;0;0;0;0;0;0;0;0;0;0;0;0'
    		)
        

    def test_meal_evaluator(self):
        meal = Meal.objects.get(meal_name = "TestMeal")
        result = evaluate_meal(meal, ["2"])
        print(result)
        